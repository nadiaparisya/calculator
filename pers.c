#include <stdio.h>
#include <math.h>

int main()
{
    int x, count;
    printf("   Mencari Persamaan Kuadrat P(x) = x^4+7x^3-5x+9  \n");
    printf(" Masukan Nilai x = ");	
    scanf("%d", &x);
    count = pow(x,4) + (7*pow(x,3)) - (5*x) + 9;
    printf ("hasilnya = %d", count);
}