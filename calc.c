#include<stdio.h>
#include<stdlib.h>

// main function
int main()
{
  float number;
  float result = 0;
  int menu;

    printf("Enter number: ");
    scanf("%f", &result);

    printf("\n*****************");
    printf("\n1.Addition");
    printf("\n2.Subtraction");
    printf("\n3.Multiplication");
    printf("\n4.Division");
    printf("\n5.Remainder");
    printf("\n6.Result");
    printf("\nEnter your choice: ");
    scanf("%d", &menu);

    while(menu !=6){
    switch (menu) {
    // function for addition
      case 1:
        printf("Addition \n");
        printf("Enter Number : ");
        scanf("%f",&number);
        result = result + number;
        break;

    // function for substraction
      case 2:
        printf("Subtract \n");
        printf("Enter Number : ");
        scanf("%f",&number);
        result = result - number;
        break;

     // function for multiplication
      case 3:
        printf("Multiply \n");
        printf("Enter Number : ");
        scanf("%f",&number);
        result = result * number;
        break;

    // function for division
      case 4:
        printf("Divide \n");
        printf("Enter Number : ");
        scanf("%f",&number);
        result = result / number;
        break;

    // function for remainder
      case 5:
        printf("Remainder\n");
        printf("Enter Number : ");
        scanf("%f",&number);
        result = (int)result % (int)number;
        break;
    
    }
    
      printf("Choose Menu : ");
        scanf("%d",&menu);
    }
    
    printf("result: %0.2f", result);
    printf("\n-------------------------------\n");
    printf("This is the result, Thank You.");
        
    return 0;
}